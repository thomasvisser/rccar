//
//  AboutTableViewController.swift
//  RcCars
//
//  Created by Thomas Visser on 21-12-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit

class AboutTableViewController: UITableViewController {

    let tableCellStyleName: [String] = ["App", "Version", "Author", "Company"]
    let tableCellStyleDetail: [String] = ["RC Cars", "1.0", "Thomas Visser", "The V inc."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.allowsSelection = false
        self.tableView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableCellStyleName.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutCell", for: indexPath) as! AboutTableViewCell
        
        cell.nameLabel.text = tableCellStyleName[indexPath.row]
        cell.detailLabel.text = tableCellStyleDetail[indexPath.row]
        
        loadColor(cell: cell)
        
        return cell
    }
    
    func loadColor(cell: AboutTableViewCell){
        let saveIt = SaveAppInfo()

        var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()
        UIColors = saveIt.loadColor()
        
        for key in UIColors.keys{
            if key == "textColor"{cell.nameLabel.textColor = UIColors[key]}
            else if key == "detailTextColor"{cell.detailLabel.textColor = UIColors[key]}
            else if key == "backgroundColor"{self.view.backgroundColor = UIColors[key]}
            else if key == "rowBackgroundColor"{cell.backgroundColor = UIColors[key]}
            else if key == "seperator"{self.tableView.separatorColor = UIColors[key]}
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

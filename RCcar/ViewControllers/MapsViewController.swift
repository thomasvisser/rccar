//
//  MapsViewController.swift
//  RcCars
//
//  Created by Thomas Visser on 27-11-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit
import CoreLocation
import Mapbox
import SystemConfiguration.CaptiveNetwork

class MapsViewController: UIViewController, CLLocationManagerDelegate, MGLMapViewDelegate, UITabBarControllerDelegate, UIPopoverPresentationControllerDelegate{


    @IBOutlet weak var UImapView: UIView!
    let mappie = MapDownloader.map
    
    weak var car: Car?
    var carMarker = MGLPointAnnotation()
    var startMarker = MGLPointAnnotation()
    var carPath = [String: CLLocationCoordinate2D]()
    var padje = [CLLocationCoordinate2D]()
    var polyline: MGLPolyline?
    var totalCoordinates = 0
    var mapsMenuColor: UIColor = UIColor.white
    var updateAllowed = true
    
    var showGPS = false
    let locationManager = CLLocationManager()
    var gpsQueue = OperationQueue()
    var isUpdateData = true
    var nightMode = false
    
    var timer: Timer?
    
    var lastRecord: String = "1"
    var dictCounter: String = "1"
    var setDictCounter = true
    
    var dummyModeEnabled: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mappie.frame = self.UImapView.bounds
        mappie.delegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        self.tabBarController?.delegate = self
        
        setupMap()
        
        //Get Dummy Mode
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.dummyModeEnabled = appDelegate.dummyMode
    }

    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear Map")
        self.tabBarController?.delegate = self
        
        let ssid = fetchSSIDInfo()
        print("SSID: \(ssid)")
        
        print("packs count \(MGLOfflineStorage.shared().packs?.count)")
        if MGLOfflineStorage.shared().packs?.count == 0 && ssid.contains("Macbook"){
            let alert = UIAlertController(title: "No Map Data Available", message: "Download a map for offline use first with an active internet connection.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style:
                UIAlertActionStyle.default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    return
            }))
            
            let feedback = UINotificationFeedbackGenerator()
            self.present(alert, animated: true, completion: nil)
            feedback.notificationOccurred(.error)
        }
        
        if MGLOfflineStorage.shared().packs?.count != nil{
        }
        
        updateAllowed = true
        
        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
        if(!dummy){
            gpsInQueue()
        }
        
        loadColor()
        checkNightMode()
    }
    
    func fetchSSIDInfo() ->  String {
        var currentSSID = ""
        if let interfaces:CFArray = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                if unsafeInterfaceData != nil {
                    let interfaceData = unsafeInterfaceData! as Dictionary!
                    for dictData in interfaceData! {
                        if dictData.key as! String == "SSID" {
                            currentSSID = dictData.value as! String
                        }
                    }
                }
            }
        }
        return currentSSID
    }
    
    func setupMap(){
        
        mappie.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        mappie.showsUserLocation = true
        mappie.delegate = self
        
        let gpsPos = locationManager.location?.coordinate
    
        guard let pos = gpsPos else {print("Error when loading GPS pos"); return}
        
        let long = pos.longitude
        let lat = pos.latitude
        print("longitude \(long) latitude \(lat)")
        
        mappie.setCenter(CLLocationCoordinate2D(latitude: pos.latitude, longitude: pos.longitude), zoomLevel: 11.5, animated: false)
        
        self.UImapView.addSubview(mappie)
    }
    
    func gpsInQueue(){
        let taskUpdateGPS = BlockOperation(block: {() -> Void in
            self.updateGPS()
        })
        gpsQueue.addOperation(taskUpdateGPS)
        
        print("Operation in queue \(gpsQueue.operationCount)")
    }
    
    // MARK: IBActions
    @IBAction func infoButton(_ sender: UIBarButtonItem) {
        
        //Create Action Sheet
        let mapsMenu = UIAlertController(title: nil, message: "Choose Map Style", preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "Default", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.mappie.styleURL = MGLStyle.streetsStyleURL(withVersion: 9)
        })
        let satelliteAction = UIAlertAction(title: "Satellite", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.mappie.styleURL = MGLStyle.satelliteStyleURL(withVersion: 9)
        })
        let terrainAction = UIAlertAction(title: "Terrain", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.mappie.styleURL = MGLStyle.outdoorsStyleURL(withVersion: 9)

        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in})

        mapsMenu.addAction(defaultAction)
        mapsMenu.addAction(satelliteAction)
        mapsMenu.addAction(terrainAction)
        mapsMenu.addAction(cancelAction)
        
        //Create popover for iPad.
        let popover = mapsMenu.popoverPresentationController
        popover?.delegate = self
        popover?.barButtonItem = sender
        popover?.permittedArrowDirections = UIPopoverArrowDirection.up
        
        self.present(mapsMenu, animated: true, completion: nil)
    }
    
    func updateGPS(){
    
        while(updateAllowed){
            var tempList: [String] = []
            var message: String = ""
        
            //Send last record to server
            do{ try ClientSocket.sharedInstance.writeMessage(message: "gps")} catch{ print("Error")}
            
            do{ try ClientSocket.sharedInstance.writeMessage(message: (self.car?.name)! + ";" + self.lastRecord)} catch{ print("Error")}
     
            //Read as long as the final message is received by the socket, to make sure every data is received.
            repeat{
                let partMessage = ClientSocket.sharedInstance.readMessage()
                message += partMessage!
                
                if !isUpdateData {break}
            }while !(message.range(of: "this is the end") != nil)
            
            print("Message received \(message)")
            
            if message.characters.count == 16{
                if message.contains("this is the end"){
                    print("Returned because no GPS!")
                    return
                }
            }
            
            tempList = message.components(separatedBy: ";")
            print("Amount coordinates \(tempList.count)")
            totalCoordinates += tempList.count
            print("total coordinates \(totalCoordinates)")
            
            //Loop trough every coordinate in the list to process it and add it to the map
            for recordMess in tempList {
                sleep(UInt32(0.5))
                print("Inside foreach")
                let gpsPos = recordMess.components(separatedBy: ",")
            
                var record: Int = 0
                var latitude: Double = 0
                var longitude: Double = 0
                
                //Check if there is an actual gps position
                if gpsPos.count > 0{
                    
                    print("gpsPos.count > 0")
                    //Check if the last record needs to be processed, since the last position is the Car position
                    if gpsPos[0].contains("this is the end"){
                        
                        print("carpath size \(self.carPath.count)")
                    
                        let coordinate = self.carPath[lastRecord]
                        guard let coor = coordinate else{ print("Error"); return}
                        
                        if coor.latitude != 0 && coor.longitude != 0{
                            self.car?.location = CLLocationCoordinate2D(latitude: coor.latitude, longitude: coor.longitude)
                        }
                        
                        if let position = self.car?.location{
                            latitude = position.latitude;
                            longitude = position.longitude;
                            
                            if latitude != 0 && longitude != 0{
                                
                                // Add RC car marker to the map.
                                self.carMarker = MGLPointAnnotation()
                                
                                self.carMarker.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                                self.carMarker.title = "Rc Car"
                            }
                        }
                        break
                    }
                    else if gpsPos[0] != "NoMessage" && gpsPos.count >= 3{
                        
                        record = Int((gpsPos[0] as NSString).intValue)
                        latitude = (gpsPos[1] as NSString).doubleValue
                        longitude = (gpsPos[2] as NSString).doubleValue
                        
                        self.lastRecord = String(record)
                        
                        if record == 1{
                            // Add RC car marker to the map.
                            self.startMarker = MGLPointAnnotation()
                            
                            self.startMarker.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                            self.startMarker.title = "Start"
                            self.startMarker.subtitle = "Rc Car start position"
                        }
                        
                        if latitude != 0 && longitude != 0{
                            print("latitude \(latitude) longitude \(longitude)"); print("count carPath \(self.carPath.count)")
                            
                            self.carPath["\(record)"] = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                            
                            if self.setDictCounter{
                                
                                
                                self.carPath.removeValue(forKey: "0")
                                guard let minKey = self.carPath.keys.min() else { print("Error min key"); return}
                                self.dictCounter = minKey
                                self.setDictCounter = false
                            }
                        }
                        
                        for (key, value) in self.carPath{
                            if key == dictCounter{
                                padje.append(value);
                                dictCounter = "\(Int(dictCounter)! + 1)"
                            }
                            else if key == String(record) {
                                break
                            }
                        }
                        
                        if var map = self.polyline{
                            self.polyline = nil
                            map = MGLPolyline(coordinates: &padje, count: UInt(self.carPath.count))
                        }
                        else{
                            self.polyline = MGLPolyline(coordinates: &padje, count: UInt(self.carPath.count))
                        }
                    }
                }
            }
            sleep(UInt32(2)); //updateAllowed = false
        }
    }
    
    func loadColor(){
        let saveIt = SaveAppInfo()
        
        var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()        
        UIColors = saveIt.loadColor()

        for key in UIColors.keys{
            if key == "backgroundColor"{self.mapsMenuColor = UIColors[key]!;
                
                if(UIColors[key].debugDescription.contains("0.260")){
                    nightMode = true
                }else{
                    nightMode = false
                }
            }
        }
    }
    
    func checkNightMode(){
        
        if nightMode{
            // Set the map style to dark mode.
            self.mappie.styleURL = MGLStyle.darkStyleURL(withVersion: 9)

        }
        else{
            // Switch back to normal view
            self.mappie.styleURL = MGLStyle.streetsStyleURL(withVersion: 9)
        }
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        mapView.selectAnnotation(annotation, animated: true)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        print("Changed tabs from Maps")
        if !(viewController is MapsViewController){
            self.isUpdateData = false
            gpsQueue.cancelAllOperations()
            self.updateAllowed = false
            
            if let clock = timer{
                clock.invalidate()
            }
            
            guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return false}
            if(!dummy){
                do{ try ClientSocket.sharedInstance.writeMessage(message: "stop gps") } catch{ print("Error")}
            }
            
            print("stopped operations")
        }
        return true
    }

    
    /*// MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    }*/
}

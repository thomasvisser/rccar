//
//  SettingsTableViewController.swift
//  RcCars
//
//  Created by Thomas Visser on 01-12-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit

class InfoTableViewController: UITableViewController, UITabBarControllerDelegate {

    let displayData: [String] = ["Car Name", "Model Number", "Firmware Version", "Wifi Network", "Total distance"]
    var data: [String] = ["dummy Car" , "dummy Model", "dummy Firmware", "dummy Version", "dummy Wifi"]
    
    var dummyModeEnabled: Bool?
    
    override func viewDidLoad() {
        
        print("ViewDidLoad Info")
        
        super.viewDidLoad()
        self.tabBarController?.delegate = self
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.allowsSelection = false
        self.tableView.isScrollEnabled = false
        
        //Get Dummy Mode
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.dummyModeEnabled = appDelegate.dummyMode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("ViewWillAppear Info")

        self.tabBarController?.delegate = self
        
        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
        if(!dummy){
            do{ try ClientSocket.sharedInstance.writeMessage(message: "get data")}catch{print("Error")}
            updateData()
        }
    }

    // MARK: - TableView data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as! InfoTableViewCell
        
        cell.nameLabel.text = displayData[indexPath.row]
        cell.detailLabel.text = data[indexPath.row]
        
        loadColor(cell: cell)
        return cell
    }
    
    func updateData(){
        
        let readData = ClientSocket.sharedInstance.readMessage()
        var splitData = [String]()
        
        if let message = readData{
            splitData = message.components(separatedBy: ";")
        }
                
        if splitData.count == 4{
            for i in 0..<4{
                data.insert(splitData[i], at: i);            }
            
            self.tableView.reloadData()
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        print("Changed Tabs from Info")
        return true
    }
    
    func loadColor(cell: InfoTableViewCell){
        let saveIt = SaveAppInfo()

        var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()
        UIColors = saveIt.loadColor()
        
        for key in UIColors.keys{
            if key == "separator"{self.tableView.separatorColor = UIColors[key]}
            else if key == "textColor"{cell.nameLabel.textColor = UIColors[key]}
            else if key == "detailTextColor"{cell.detailLabel.textColor = UIColors[key]}
            else if key == "rowBackgroundColor"{cell.backgroundColor = UIColors[key]}
            else if key == "backgroundColor"{self.tableView.backgroundColor = UIColors[key]}
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

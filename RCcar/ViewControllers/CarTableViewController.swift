//
//  CarTableViewController.swift
//  RcCars
//
//  Created by Thomas Visser on 17-11-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit
import CoreLocation

class CarTableViewController: UITableViewController {

    var listCars = [Car]()
    var backgroundLabel: UILabel?
    var backgroundViewColor: UIColor = UIColor.white
    var textColor: UIColor = UIColor.black
    var refresh: UIRefreshControl!
    
    var dummyModeEnabled: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresh = UIRefreshControl()
        refresh.backgroundColor = UIColor.white
        refresh.tintColor = UIColor.black
        refresh.backgroundColor = self.backgroundViewColor
        refresh.addTarget(self, action: #selector(CarTableViewController.resfreshList(_:)), for: UIControlEvents.valueChanged)

        tableView.addSubview(refresh)
        
        //Get Dummy Mode
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.dummyModeEnabled = appDelegate.dummyMode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {

        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
        if(!dummy){

            if parent == nil{
                do{
                    try ClientSocket.sharedInstance.writeMessage(message: "quit")
                }
                catch{
                    print("Error!")
                }
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        self.backgroundLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        
        if listCars.count != 0{
            self.tableView.backgroundView = nil
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
            self.backgroundLabel?.removeFromSuperview()
            return 1
        }
        else {
            self.backgroundLabel?.text = "No Cars available"
            self.backgroundLabel?.textColor = UIColor.black
            self.backgroundLabel?.textAlignment = NSTextAlignment.center
            self.backgroundLabel?.font = UIFont(name: "Palatino-Italic", size: 20)

            self.backgroundLabel?.backgroundColor = self.backgroundViewColor
            self.backgroundLabel?.textColor = self.textColor
            self.tableView.backgroundView = backgroundLabel
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCars.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCarCell", for: indexPath) as! CarTableViewCell
        
        cell.carImage.image = listCars[indexPath.row].image
        cell.carName.text = listCars[indexPath.row].name
        cell.carModelNumber.text = listCars[indexPath.row].carID
        
        loadColor(cell: cell)
        
        return cell
    }
    
    //Mark: Actions
    @IBAction func resfreshList(_ sender: UIBarButtonItem) {
        
        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
        if(!dummy){
            do{
                try ClientSocket.sharedInstance.writeMessage(message: "refresh")
            }
            catch{
                print("Error!")
            }
        }
        
        usleep(1000)
        
        if(!dummy){
            updateList()
        }
        
        self.tableView.reloadData()
        self.refresh?.endRefreshing()
    }
    
    func updateList(){

        var newCarList = [Car]()
        
        let carList = ClientSocket.sharedInstance.readMessage()
        
        if let list = carList {
            let ipList = list.components(separatedBy: ",")
            
            for ip in ipList {
                let carID = ip.components(separatedBy: "=")
                print(carID)
                
                if carID.count > 2{
                    newCarList.append(Car(name: carID[2], image: #imageLiteral(resourceName: "RaceCar"), carID: carID[0], ip: carID[1]))
                }
            }
        }
        listCars = newCarList
    }
    
    func loadColor(cell: CarTableViewCell){
        let saveIt = SaveAppInfo()
        
        var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()

        UIColors = saveIt.loadColor()
        
        for key in UIColors.keys{
            if key == "textColor"{
                cell.carName.textColor = UIColors[key]
                cell.carModelNumber.textColor = UIColors[key]
                self.textColor = UIColors[key]!
            }
            else if key == "rowBackgroundColor"{cell.backgroundColor = UIColors[key]}
            else if key == "backgroundColor"{
                self.tableView.backgroundColor = UIColors[key]
                self.backgroundViewColor = UIColors[key]!
            }
            else if key == "seperator"{self.tableView.separatorColor = UIColors[key]}
        }
    }
    
    // MARK: - Navigation
    
    //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "carConnected"{
            print("destination \(segue.destination)")
            if segue.destination is UITabBarController{
                if let selectedcell = sender as? CarTableViewCell {
                    let indexPath = tableView.indexPath(for: selectedcell)!
                    let selectedCar = listCars[indexPath.row]
                    
                    if let barViewController = segue.destination as? UITabBarController{
                        if let carViewController = barViewController.viewControllers?[0] as? UINavigationController{
                            if let destViewController = carViewController.viewControllers[0] as? CarViewController{
                                
                                destViewController.car = selectedCar
                                
                                guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
                                if(!dummy){
                            
                                    do{
                                        try ClientSocket.sharedInstance.writeMessage(message: selectedCar.carID)
                                    }
                                    catch{
                                        print("Error!")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
}

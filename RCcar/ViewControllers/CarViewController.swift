//
//  CarViewController.swift
//  RcCars
//
//  Created by Thomas Visser on 17-11-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit
import CoreMotion
import Darwin
import CDJoystick
import Mapbox

class CarViewController: UIViewController, UINavigationControllerDelegate, UITabBarControllerDelegate, UIWebViewDelegate {
    
    var message: String?
    var timer: Timer?
    var car: Car?
    var updateAllowed = true
    //var thisView = "test"
    var permanentStop = false
    
    var dummyModeEnabled: Bool?
    
    //Sensor and label data
    var leftVelocity: (x: Float, y: Float)?
    var rightVelocity: (x: Float, y: Float)?
    var gyroData: (x: Double?, y: Double?, z: Double?)?
    var leftAngle: Float = 0.0
    var rightAngle: Float = 0.0
    var leftLastMessage: String?
    var rightLastMessage: String?
    var isDebugMode = true
    var lastMessage: String?
    var textColor = UIColor.black
    var feedBackAllowed: Bool = false
    var counter = 0
    
    var driveBackQueue = OperationQueue()
    public var feedbackQueue = OperationQueue()
    
    @IBOutlet weak var videoStream: UIWebView!
    
    @IBOutlet weak var goBackButton: UIButton!
    var goBackFrame:CGRect?
    
    //actual labels
    @IBOutlet weak var testLabel: UILabel!
    @IBOutlet weak var leftVelocityLabel: UILabel!
    @IBOutlet weak var leftAngleLabel: UILabel!
    @IBOutlet weak var rightVelocityLabel: UILabel!
    @IBOutlet weak var rightAngleLabel: UILabel!
    @IBOutlet weak var joystickDescription: UILabel!
    @IBOutlet weak var gyroXlabel: UILabel!
    @IBOutlet weak var gyroYlabel: UILabel!
    @IBOutlet weak var gyroZlabel: UILabel!
    
    @IBOutlet weak var dummyAngleLabel: UILabel!
    @IBOutlet weak var dummyVelocityLabel: UILabel!
    @IBOutlet weak var dummyVelocityLabel2: UILabel!
    @IBOutlet weak var dummyAngleLabel2: UILabel!
    
    let leftJoystick = CDJoystick()
    let rightJoystick = CDJoystick()
    
    var motionManager: CMMotionManager!

    override func viewDidLoad() {
        //print("in ViewDidLoad")
        super.viewDidLoad()
        
        //Get Dummy Mode
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.dummyModeEnabled = appDelegate.dummyMode
        
        if let auto = self.car{
            self.message = "This is car \(auto.carID)"
        }
        
        initJoystick()
        testLabel.text = self.message
        self.leftVelocity = (x: 0.0, y: 0.0)
        self.leftAngle = 0.0
        self.rightVelocity = (x: 0.0, y: 0.0)
        self.rightAngle = 0.0
        
        self.goBackFrame = self.goBackButton.frame
        
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.alpha = 0.99
        self.tabBarController?.tabBar.tintColor = #colorLiteral(red: 0, green: 0.7741906047, blue: 0.3090999424, alpha: 1)
        self.extendedLayoutIncludesOpaqueBars = true
        
        let mapNavView = self.tabBarController?.viewControllers?[2] as? UINavigationController
        let destView = mapNavView?.viewControllers[0] as? MapsViewController
        
        if let currentCar = self.car{
            destView?.car = currentCar
        }
        
        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
        if(!dummy){
            //settings for the UIWebView
            self.videoStream.delegate = self
            let url = URL(string: "http://192.168.42.16/html/min.php")
            let request = URLRequest(url: url!)
            videoStream.loadRequest(request)
            videoStream.stringByEvaluatingJavaScript(from: "window.alert=null;")
            let viewSize: CGSize = self.view.bounds.size
            videoStream.scrollView.zoomScale = viewSize.width
            videoStream.scrollView.contentSize = viewSize
            videoStream.scrollView.isUserInteractionEnabled = true
        }
        
        //Hide all the debug info from the ViewController if debug mode is disabled
        if !isDebugMode {
            self.testLabel.isHidden = true
            self.leftVelocityLabel.isHidden = true
            self.leftAngleLabel.isHidden = true
            self.rightVelocityLabel.isHidden = true
            self.rightAngleLabel.isHidden = true
            self.gyroXlabel.isHidden = true
            self.gyroYlabel.isHidden = true
            self.gyroZlabel.isHidden = true
            self.dummyVelocityLabel.isHidden = true
            self.dummyAngleLabel.isHidden = true
            self.dummyVelocityLabel2.isHidden = true
            self.dummyAngleLabel2.isHidden = true
        }
        else if isDebugMode{
            videoStream.isHidden = true
        }
        
        motionManager = CMMotionManager()
        checkRotation()
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        print("in viewWillAppear")
        
        if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft ||
            UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight {
            
            startAcceleroMeasurement(); print("Started Accellero!!")
            
            let image = UIImage(named: "Return") as UIImage?
            self.goBackButton.alpha = 0.6
            self.goBackButton.setImage(image, for: .normal)
        }
        
        self.tabBarController?.delegate = self
        
        print("permanent stop: \(permanentStop)")
        if !permanentStop{
            if self.timer == nil{
                self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(CarViewController.updateData), userInfo: nil, repeats: true); print("Timer started again in ViewWillAppear")
            }
        
            UIDevice.current.beginGeneratingDeviceOrientationNotifications()
            NotificationCenter.default.addObserver(self, selector: #selector(CarViewController.checkRotation), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        
            print("feedback count \(feedbackQueue.operationCount)")
            if feedbackQueue.operationCount == 0{
                let setFeedBack = BlockOperation(block: {() -> Void in
                    self.setFeedback()
                })
                feedbackQueue.addOperation(setFeedBack); print("Added feedback operation to queue")
            }
        }
    
        loadColor()
        initUI()
        checkRotation()
        
        //debug
        /*self.thisView = "View1!"
        print("view: \(self.thisView)")*/
    }
    
    func initUI(){

        self.testLabel.textColor = self.textColor
        self.leftVelocityLabel.textColor = self.textColor
        self.leftAngleLabel.textColor = self.textColor
        self.rightVelocityLabel.textColor = self.textColor
        self.rightAngleLabel.textColor = self.textColor
        self.joystickDescription.textColor = self.textColor
        self.gyroXlabel.textColor = self.textColor
        self.gyroYlabel.textColor = self.textColor
        self.gyroZlabel.textColor = self.textColor
        self.dummyAngleLabel.textColor = self.textColor
        self.dummyVelocityLabel.textColor = self.textColor
        self.dummyVelocityLabel2.textColor = self.textColor
        self.dummyAngleLabel2.textColor = self.textColor
    }
    
    // MARK: Initialize Joystick
    func initJoystick(){
        
        //Joystick Setup
        leftJoystick.frame = CGRect(x: 40, y: (self.view.bounds.height / 2) - 50, width: 100, height: 100)
        leftJoystick.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        leftJoystick.substrateColor = #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 1)
        leftJoystick.substrateBorderColor = #colorLiteral(red: 0.5723067522, green: 0.5723067522, blue: 0.5723067522, alpha: 1)
        leftJoystick.substrateBorderWidth = 1.0
        leftJoystick.stickSize = CGSize(width: 50, height: 50)
        leftJoystick.stickColor = #colorLiteral(red: 0.4078193307, green: 0.4078193307, blue: 0.4078193307, alpha: 1)
        leftJoystick.stickBorderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        leftJoystick.stickBorderWidth = 2.0
        leftJoystick.fade = 0.5

        view.addSubview(leftJoystick)
        
        rightJoystick.frame = CGRect(x: 274, y: (self.view.bounds.height / 2) - 50, width: 100, height: 100)
        rightJoystick.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        rightJoystick.substrateColor = #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 1)
        rightJoystick.substrateBorderColor = #colorLiteral(red: 0.5723067522, green: 0.5723067522, blue: 0.5723067522, alpha: 1)
        rightJoystick.substrateBorderWidth = 1.0
        rightJoystick.stickSize = CGSize(width: 50, height: 50)
        rightJoystick.stickColor = #colorLiteral(red: 0.4078193307, green: 0.4078193307, blue: 0.4078193307, alpha: 1)
        rightJoystick.stickBorderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        rightJoystick.stickBorderWidth = 2.0
        rightJoystick.fade = 0.5
        
        view.addSubview(rightJoystick)
    }
    
    func startAcceleroMeasurement(){
        
        //print("startAccelero")

        //Starts recieving Accelerometer updates and updates the values
        if self.motionManager.isAccelerometerAvailable {
            self.motionManager.accelerometerUpdateInterval = 0.08
            self.motionManager.startAccelerometerUpdates(to: OperationQueue.main){
                (data: CMAccelerometerData?, error: Error?) in
                if let gyro = data?.acceleration {
                    
                    //round the values at 9 digit precision
                    let gyroX = Double(round(1000000000*gyro.x)/1000000000)
                    let gyroY = Double(round(1000000000*gyro.y)/1000000000)
                    let gyroZ = Double(round(1000000000*gyro.z)/1000000000)
                    
                    self.gyroData = (x: gyroX, y: gyroY, z: gyroZ)
                    
                    if self.isDebugMode{
                        self.gyroXlabel.text = "x: \(gyroX)";
                        self.gyroYlabel.text = "y: \(gyroY)";
                        self.gyroZlabel.text = "z: \(gyroZ)";
                        //print("Gyro X: \(gyroX)\tGyro Y: \(gyroY)\tGyro Z: \(gyroZ)")
                    }
                }
                self.sendToSocket(allowGyro: true)
                sleep(UInt32(0.05))
            }
        }
    }
    
    // MARK: Actions
    
    @IBAction func sendCarBack(_ sender: UIButton) {
        
        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
        if(!dummy){
            do{ try ClientSocket.sharedInstance.writeMessage(message: "go home")}catch{print("ERROR")}
        }
        
        //stop any processes currently active
        if let clock = self.timer{
            clock.invalidate()
            NotificationCenter.default.removeObserver(self)
        }
        motionManager.stopAccelerometerUpdates()
        
        let alert = UIAlertController(title: "Car is returning to it's original position", message: "The car can not be controller during this process. Press \"Cancel\" to undo this action", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) -> Void in
            
            guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
            if(!dummy){
                do{ try ClientSocket.sharedInstance.writeMessage(message: "Cancel Return Car")}catch{ print("Error")}
            }
            
            //Continue timer and processes since user hit cancel
            self.startAcceleroMeasurement()
            
            if self.timer == nil{
                self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(CarViewController.updateData), userInfo: nil, repeats: true); print("Timer started again in ViewWillAppear")
            }
            self.checkRotation()
            self.driveBackQueue.cancelAllOperations()
        }))
        self.present(alert, animated: true, completion: nil)

        //wait for car to regain control to the user
        let waitForCar = BlockOperation(block: {() -> Void in
            
            guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
            if(!dummy){
                repeat{
                    let message = ClientSocket.sharedInstance.readMessage()
                    
                    if let mess = message{
                        if mess.contains("Car is back"){
                            
                            //Continue timer and processes
                            self.startAcceleroMeasurement()
                 
                            if self.timer == nil{
                                self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(CarViewController.updateData), userInfo: nil, repeats: true); print("Timer started again in ViewWillAppear")}
                            self.checkRotation()
                            
                            self.dismiss(animated: true, completion: nil)
                            break;
                        }
                    }
                }while true
            }else{
                sleep(5)
                
                //Continue timer and processes
                self.startAcceleroMeasurement()
                
                if self.timer == nil{
                    self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(CarViewController.updateData), userInfo: nil, repeats: true); print("Timer started again in ViewWillAppear")}
                //self.checkRotation()
                
                self.dismiss(animated: true, completion: nil)
            }
            
            print("Canceled drive back!")
        })
        driveBackQueue.addOperation(waitForCar)
    }
    
    @objc func checkRotation(){

        //print("in checkrotation")

        //Check if Interface rotation is in Landscape, since the joystick positions need to be altered
        if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft ||
            UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight {
            
            print("Inside CheckRotation for Landscape")
            startAcceleroMeasurement()
            
            //Change layout for the return button
            let image = UIImage(named: "Return") as UIImage?
            self.goBackButton.alpha = 0.6
            self.goBackButton.setImage(image, for: .normal)
            self.goBackButton.setTitle(nil, for: .normal)

            //Check if device is an iPhone or iPad and adjust joystick positions
            if self.view.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.compact {
                //iPhone landscape
                leftJoystick.frame = CGRect(x: 40, y: 220, width: 100, height: 100)
                rightJoystick.frame = CGRect(x: 596, y: 220, width: 100, height: 100)
                
            }
            else if self.view.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular{
                //iPhone Portrait and iPad Portrait/Landscape
                leftJoystick.frame = CGRect(x: 40, y: 530, width: 100, height: 100)
                rightJoystick.frame = CGRect(x: 884, y: 530, width: 100, height: 100)
            }
            
            if !isDebugMode{
                videoStream.isHidden = false
            }
            
            joystickDescription.isHidden = true
            
            if !permanentStop{
                if self.timer == nil{
                    self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(CarViewController.updateData), userInfo: nil, repeats: true); print("timer started again in checkRotation")
                }
            }
        }
        else if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.portrait {
            initJoystick()
            joystickDescription.isHidden = false
            
            //Change layout for the return button
            self.goBackButton.alpha = 1.0
            self.goBackButton.setImage(nil, for: .normal)
            self.goBackButton.setTitle("Send Car Back", for: .normal)
            
            if let frame = self.goBackFrame{
                self.goBackButton.frame = frame
            }
            
            //check if device is iPad
            if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
                leftJoystick.frame = CGRect(x: 80, y: 730, width: 100, height: 100)
                rightJoystick.frame = CGRect(x: 588, y: 730, width: 100, height: 100)
            }
            
            videoStream.isHidden = true
        }
    }
    
    @objc func updateData(){
        
        //Update values for left Joystick
        if !leftJoystick.isReset {
            leftJoystick.trackingHandler = { joystickData in
                self.leftVelocity = (x: Float(joystickData.velocity.x), y: Float(joystickData.velocity.y))
                self.leftAngle = Float(joystickData.angle)
            }
        }
        else {
            self.leftVelocity = (x: 0, y: 0)
            self.leftAngle = Float(0.0)
        }
        
        //Update values for right joystick
        if !rightJoystick.isReset {
            rightJoystick.trackingHandler = { joystickData in
                self.rightVelocity = (x: Float(joystickData.velocity.x), y: Float(joystickData.velocity.y))
                self.rightAngle = Float(joystickData.angle)
            }
        }
        else {
            self.rightVelocity = (x: 0, y: 0)
            self.rightAngle = Float(0.0)
        }
        
        
        //Update the labels in the UI
        if self.leftVelocity != nil{
            if ((self.leftVelocity?.x)! != Float(0) && (self.leftVelocity?.y)! != Float(0)){
                //Update labels when debug mode enabled
                if isDebugMode{
                    leftVelocityLabel.text = "x: " + String(describing: self.leftVelocity!.x) + " y: " + String(describing: self.leftVelocity!.y)
                    leftAngleLabel.text = String(self.leftAngle)
                }
                
                sendToSocket(allowGyro: false)
            }
        }
        
        if self.rightVelocity != nil{
            if ((self.rightVelocity?.x)! != Float(0) && (self.rightVelocity?.y)! != Float(0)){
                //Update labels when debug mode enabled
                if isDebugMode{
                    rightVelocityLabel.text = "x: " + String(describing: self.rightVelocity!.x) + " y: " + String(describing: self.rightVelocity!.y)
                    rightAngleLabel.text = String(self.rightAngle)
                }
                
                sendToSocket(allowGyro: false)
            }
        }
    }
    
    func sendToSocket(allowGyro: Bool){
        
        //print("in sendToSocket")
        
        /*left: x = -1.0        right: x = 1.0
          forward: y = -1.0     backward: y = 1.0
         */
        let socketLeftVelocity = (x: self.leftVelocity!.x * 100, y: self.leftVelocity!.y * 100)
        let socketRightVelocity = (x: self.rightVelocity!.x * 100, y: self.rightVelocity!.y * 100)
        
        var leftMessage: String?
        var rightMessage: String?
        
        //Check the values for the left Joystick to determine forward and backward movement
        if socketLeftVelocity.x > -10 && socketLeftVelocity.x < 10 {
            if socketLeftVelocity.y > 0 {
                leftMessage = "backward:\(socketLeftVelocity.y)"
                self.leftLastMessage = "backward"
            }
            else if socketLeftVelocity.y < 0 {
                leftMessage = "forward:\(abs(socketLeftVelocity.y))"
                self.leftLastMessage = "forward"
            }
            else {
                //print("Stop")
                
                if self.leftLastMessage != "stop"{
                    leftMessage = "stop"
                    self.leftLastMessage = "stop"
                }
                else {
                    leftMessage = nil
                }
            }
        }
        
        if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.portrait{
            
            //Check the values for the right Joystick to determine left and right movement
            if socketRightVelocity.y > -10 && socketRightVelocity.y < 10{
                if socketRightVelocity.x > 0 {
                    rightMessage = "right:\(socketRightVelocity.x)"
                    self.rightLastMessage = "right"
                }
                else if socketRightVelocity.x < 0 {
                    rightMessage = "left:\(abs(socketRightVelocity.x))"
                    self.rightLastMessage = "left"
                }
                else{
                    //print("Stop")
                    
                    if self.rightLastMessage != "stop"{
                        rightMessage = "stop"
                        self.rightLastMessage = "stop"
                    }
                    else {
                        rightMessage = nil
                    }
                }
            }
        }
        else if (UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft ||
            UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight) && allowGyro {
            
            //Calculate all the correct values from the core Accelerometer Data

            var Accelero: (x: Double?, y: Double?, z: Double?) = (0, 0, 0)
            
            //make sure gyroData not contains nill values
            guard let gData = gyroData else {
                print("No Gyrodata available"); return
            }
            
            guard let gyroX = gData.x, let gyroY = gData.y, let gyroZ = gData.z else {
                print("No Gyrodata available, no X, no Y, no Z"); return
            }
            
            Accelero = (x: gyroX, y: gyroY, z: gyroZ)
            
            if let AcX = Accelero.x, let AcY = Accelero.y, let AcZ = Accelero.z{
                var socketAcceleroLeft = (x: Float((AcX + 0.1) * -450), y: Float((AcY + 0.1) * -450), z: Float((AcZ + 0.1) * -450))
                var socketAcceleroRight = (x: Float((AcX - 0.1) * 450), y: Float((AcY - 0.1) * 450), z: Float((AcZ - 0.1) * 450))
 
            
                //reverse left and right if landscape orientation changed
                if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight{
                    let socketLeftCopy = socketAcceleroLeft
                    socketAcceleroLeft = socketAcceleroRight
                    socketAcceleroRight = socketLeftCopy
                }
                
                //Check the values for the right Joystick to determine left and right movement
                if socketAcceleroLeft.y > 0 && socketAcceleroLeft.y < 100{
                    rightMessage = "left:\(socketAcceleroLeft.y)"
                    self.rightLastMessage = "left"
                }
                else if socketAcceleroRight.y > 0 && socketAcceleroRight.y < 100{
                    rightMessage = "right:\(abs(socketAcceleroRight.y))"
                    self.rightLastMessage = "right"
                }
                else if socketAcceleroLeft.y < 0 || socketAcceleroRight.y < 0{
                    
                    if self.rightLastMessage != "stop"{
                        print("Stop")
                        rightMessage = "stop"
                        self.rightLastMessage = "stop"
                    }
                    else {
                        rightMessage = nil
                    }
                }
                
                if socketAcceleroLeft.y > 100 {
                    
                    //Generate feedback to inform user it has reached the limit
                    
                    if self.feedBackAllowed{
                        let feedback = UIImpactFeedbackGenerator(style: .heavy)
                        feedback.impactOccurred()
                    }
                    
                    //leftMessage = "left:\(abs(socketAcceleroRight.y))"
                    self.rightLastMessage = "left"
                }
                else if socketAcceleroRight.y > 100 {
                    //Generate feedback to inform user it has reached the limit
                    
                    if self.feedBackAllowed{
                        let feedback = UIImpactFeedbackGenerator(style: .heavy)
                        feedback.impactOccurred()
                    }

                    //rightMessage = "right:\(socketAcceleroLeft.y)"
                    self.rightLastMessage = "right"
                }
            }
        }
        
        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
        if(!dummy){
            //send to socket
            if leftMessage != nil{
                print(leftMessage!)
            
                do{ try ClientSocket.sharedInstance.writeMessage(message: leftMessage!) } catch{ print("Error")}
                
                if message == "stop"{
                    self.leftLastMessage = "stop"
                }
            }
            else{
                print("Left NULL")
            }
     
            if rightMessage != nil{
                print(rightMessage!)
                
                do{ try ClientSocket.sharedInstance.writeMessage(message: rightMessage!) } catch{ print("Error")}
                if rightMessage == "stop" {
                    self.rightLastMessage = "stop"
                }
            }
            else{
                print("Right NULL")
            }
        }
    }
    
    func loadColor(){
        let saveIt = SaveAppInfo()
        
        var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()
        UIColors = saveIt.loadColor()
        
        for key in UIColors.keys{
            if key == "textColor"{self.textColor = UIColors[key]!}
            else if key == "backgroundColor"{self.view.backgroundColor = UIColors[key]}
        }
    }
    
    func setFeedback(){
        while true{
            if !self.feedBackAllowed {
                self.feedBackAllowed = true
                sleep(UInt32(1))
                self.feedBackAllowed = false
                sleep(UInt32(1))
            }
        }
    }
    
    func stopProccesses(){
        print("inside processen")
        if let clock = self.timer{
            clock.invalidate()
            self.timer = nil
            NotificationCenter.default.removeObserver(self)
            
            motionManager.stopAccelerometerUpdates()
            UIDevice.current.endGeneratingDeviceOrientationNotifications()
            feedbackQueue.cancelAllOperations()
            self.updateAllowed = false
            
            print("Stopped processen!")
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    
        print("Changed Tabs from Car")
        if !(viewController is CarViewController){
            
            guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return false}
            if(!dummy){
                do{ try ClientSocket.sharedInstance.writeMessage(message: "stop") } catch{ print("Error")}
            }
            
            if let clock = self.timer{
                clock.invalidate();
                self.timer = nil
                NotificationCenter.default.removeObserver(self)
                UIDevice.current.endGeneratingDeviceOrientationNotifications()
            }
        
            motionManager.stopAccelerometerUpdates(); print("Stopped Accelero!")
            feedbackQueue.cancelAllOperations()
            self.updateAllowed = false
            
            if viewController is MapsViewController{
                let mapView = viewController as! MapsViewController
                mapView.car = self.car
            }
        }
        return true
    }
    
    deinit {
        print("\t\t\t\t\t\t\t\t\t\t\tdeallocated method")
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Navigation

    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }*/
}

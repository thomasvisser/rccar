//
//  ViewController.swift
//  RcCars
//
//  Created by Thomas Visser on 02-11-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit
import CoreLocation
import Mapbox
import SystemConfiguration.CaptiveNetwork

class ViewController: UIViewController {
    var exceptionQueue = OperationQueue()
    var connected = false
    var progressView: UIProgressView!
    var cancelButton: UIButton!
    
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    var dummyModeEnabled: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let ssid = fetchSSIDInfo()
        //print("SSID: \(ssid)")
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackProgressDidChange), name: NSNotification.Name.MGLOfflinePackProgressChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveError), name: NSNotification.Name.MGLOfflinePackError, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkRotation), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        //Add cancel button to view, hidden by default
        self.cancelButton = UIButton()
        self.view.addSubview(self.cancelButton)
        
        self.cancelButton.setTitle("Cancel", for: .normal)
        self.cancelButton.tintColor = UIColor.green
        self.cancelButton.setTitleColor(#colorLiteral(red: 0.1432492137, green: 0.7682136893, blue: 0.3354576826, alpha: 1), for: .normal)
        
        self.cancelButton.addTarget(self, action: #selector(self.needCancel), for: .touchUpInside)
        
        self.cancelButton.isEnabled = false
        self.cancelButton.isHidden = true
        
        //Get Dummy Mode
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.dummyModeEnabled = appDelegate.dummyMode
    }
    
    func fetchSSIDInfo() ->  String {
        var currentSSID = ""
        if let interfaces:CFArray = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                if unsafeInterfaceData != nil {
                    let interfaceData = unsafeInterfaceData! as Dictionary!
                    for dictData in interfaceData! {
                        if dictData.key as! String == "SSID" {
                            currentSSID = dictData.value as! String
                        }
                    }
                }
            }
        }
        return currentSSID
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadColor()
    }
    
    func loadColor(){
        let saveIt = SaveAppInfo()
        
        var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()
        UIColors = saveIt.loadColor()
        
        for key in UIColors.keys{
            if key == "textColor"{self.infoLabel.textColor = UIColors[key]}
            else if key == "backgroundColor"{self.view.backgroundColor = UIColors[key]}
        }
    }
    
    // MARK: Rotation
    
    @objc func checkRotation(){
        let frame = self.view.bounds.size
        
        if let progress = self.progressView{
            if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.portrait {
                progress.frame = CGRect(x: frame.width / 4, y: frame.height * 0.6, width: frame.width / 2, height: 10)
                
                self.cancelButton.frame.origin.x = 136
                self.cancelButton.frame.origin.y = 445
                self.cancelButton.frame.size.width = 143
                self.cancelButton.frame.size.height = 36
                
            }
            else if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft ||
                UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight {
                progressView.frame = CGRect(x: 437.67, y: 230, width: 207, height: 10)
                
                self.cancelButton.frame.origin.x = 469.67
                self.cancelButton.frame.origin.y = 184
                self.cancelButton.frame.size.width = 143
                self.cancelButton.frame.size.height = 36
                
            }
        }
    }
    
    // Mark: UIAction
    
    @IBAction func downloadButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Download Map", message: "Make sure you have an active internet connection.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style:
            UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                
                self.downloadButton.isHidden = true
                self.downloadButton.isEnabled = false
                
                //position the cancel button correctly depending on the rotation of the device
                if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.portrait {
                    
                    self.cancelButton.frame.origin.x = 136
                    self.cancelButton.frame.origin.y = 445
                    self.cancelButton.frame.size.width = 143
                    self.cancelButton.frame.size.height = 36
                }
                else if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft ||
                    UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight {
                    
                    self.cancelButton.frame.origin.x = 469.67
                    self.cancelButton.frame.origin.y = 184
                    self.cancelButton.frame.size.width = 143
                    self.cancelButton.frame.size.height = 36
                }
                
                self.cancelButton.isEnabled = true
                self.cancelButton.isHidden = false
                
                MapDownloader.instance.downloadMap()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            return
        }))
        
        let feedback = UINotificationFeedbackGenerator()
        self.present(alert, animated: true, completion: nil)
        feedback.notificationOccurred(.error)
    }
    
    @objc func needCancel(sender: UIButton!){
        print("Need cancel!!!!")
        
        MapDownloader.instance.suspendPack()
        //MGLOfflineStorage.shared().packs?[0].suspend()
        
        progressView.removeFromSuperview()
        progressView = nil; print("progressView = null!")
        
        self.cancelButton.isEnabled = false
        self.cancelButton.isHidden = true
        
        self.downloadButton.isEnabled = true
        self.downloadButton.isHidden = false
        
        return
    }
    
    @objc func offlinePackProgressDidChange(notification: NSNotification) {
        
        // Get the offline pack and the associated user info for the pack
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String] {
            let progress = pack.progress
            let completedResources = progress.countOfResourcesCompleted
            let expectedResources = progress.countOfResourcesExpected
            
            // Calculate current progress percentage.
            let progressPercentage = Float(completedResources) / Float(expectedResources)
            
            // Setup the progress bar.
            if progressView == nil {
                print("Inside progressView")
                progressView = UIProgressView(progressViewStyle: .default)
                let frame = view.bounds.size
                
                if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.portrait {
                    progressView.frame = CGRect(x: frame.width / 4, y: frame.height * 0.6, width: frame.width / 2, height: 10)
                    
                    print("width \(frame.width / 2)")
                }
                else if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeLeft ||
                    UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight {
                    progressView.frame = CGRect(x: 437.67, y: 230, width: 207, height: 10)
                }
                
                self.view.addSubview(progressView)
            }
            
            progressView.progress = progressPercentage
            
            // If this pack has finished, print its size and resource count.
            if completedResources == expectedResources {
                let byteCount = ByteCountFormatter.string(fromByteCount: Int64(pack.progress.countOfBytesCompleted), countStyle: ByteCountFormatter.CountStyle.memory)
                print("Offline pack “\(userInfo["name"])” completed: \(byteCount), \(completedResources) resources")
                
                let alert = UIAlertController(title: "Local Map Downloaded", message: "You can now connect to the Wifi network of the car.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style:
                    UIAlertActionStyle.default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        self.dismiss(animated: true)
                }))
                progressView.removeFromSuperview()
                self.cancelButton.isEnabled = false
                self.cancelButton.isHidden = true
                
                self.downloadButton.isEnabled = true
                self.downloadButton.isHidden = false
                
                let feedback = UINotificationFeedbackGenerator()
                self.present(alert, animated: true, completion: nil)
                feedback.notificationOccurred(.error)
                
            } else {
                // Otherwise, print download/verification progress.
                print("Offline pack “\(userInfo["name"])” has \(completedResources) of \(expectedResources) resources — \(progressPercentage * 100)%.")
            }
        }
    }
    
    @objc func offlinePackDidReceiveError(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let error = notification.userInfo?[MGLOfflinePackErrorUserInfoKey] as? Error {
            print("Offline pack “\(userInfo["name"])” received error: \(error.localizedDescription)")
        }
    }
    
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "passCarList"{
            
            guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return false}
            if(!dummy){
        
                //tries to establish a connection with the host. If the socket could'nt connect, throw and display an error to the user
                do{ try ClientSocket.sharedInstance.openSocket()}
                catch{ print("Error")
                    
                    let alert = UIAlertController(title: "Car is not available", message: "Check if you are connected with the car's wifi network and if it is powered on.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style:
                        UIAlertActionStyle.default, handler: {
                            (alert: UIAlertAction!) -> Void in
                            self.dismiss(animated: true)
                    }))
                    
                    let feedback = UINotificationFeedbackGenerator()
                    self.present(alert, animated: true, completion: nil)
                    feedback.notificationOccurred(.error)
                    
                    return false
                }
            }
            return true
        }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "passCarList" {
            if let CarTableViewController = segue.destination as? CarTableViewController{
                
                var listCars = [Car]()
                
                guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
                if(!dummy){
                    
                    //Send the first messages for setup.
                    while true{
                        
                        do{
                            try ClientSocket.sharedInstance.writeMessage(message: "iDevice")
                            break
                        }
                        catch{
                            print("WRONG")
                            continue
                        }
                    }
                    usleep(200)
                
                    let carList = ClientSocket.sharedInstance.readMessage()
                    
                    if let list = carList {
                        let ipList = list.components(separatedBy: ",")
                        
                        for ip in ipList {
                            let carID = ip.components(separatedBy: "=")
                            print(carID)
                            
                            if carID.count > 2{
                                listCars.append(Car(name: carID[2], image: #imageLiteral(resourceName: "RaceCar"), carID: carID[0], ip: carID[1]))
                            }
                        }
                    }
                }
                else{
                    //Add dummy data
                    listCars.append(Car(name: "Race Car", image:#imageLiteral(resourceName: "RaceCar"), carID: "0", ip: "192.168.178.20"))
                    listCars.append(Car(name: "Race Car 1", image:#imageLiteral(resourceName: "RaceCar"), carID: "1", ip: "192.168.178.2"))
                    listCars.append(Car(name: "Race Car 2", image:#imageLiteral(resourceName: "RaceCar"), carID: "2", ip: "192.168.178.43"))
                    
                    CarTableViewController.listCars = listCars
                }
            }
        }
    }
}


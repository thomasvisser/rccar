//
//  SettingsTableViewController.swift
//  RcCars
//
//  Created by Thomas Visser on 01-12-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, UITabBarControllerDelegate, UINavigationControllerDelegate {

    let tableCellStyle: [String] = ["About", "Dark Mode", "Disconnect"]
    let defaults = UserDefaults.standard
    var isDarkModeOn: Bool = false
    var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()
    var darkSwitch: UISwitch?
    var saveIt = SaveAppInfo()
    var setter = false
    
    var dummyModeEnabled: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        //saveIt = SaveAppInfo()
        
        //Get Dummy Mode
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.dummyModeEnabled = appDelegate.dummyMode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsTableViewController.checkRotation), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        if setter == false{
            self.isDarkModeOn = saveIt.loadDarkMode()
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableCellStyle.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! SettingsTableViewCell
        
        let tabView = self.presentingViewController
        //let mainView = tabView?.presentingViewController?.childViewControllers[0]
        let mainCarView = tabView?.childViewControllers[0].childViewControllers[0] as! CarViewController
        //mainCarView.updateCarLabel()
        
        switch tableCellStyle[indexPath.row] {
            case "About":
                cell.settingsLabel.text = "About"
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                
                loadColor(cell: cell); print("loaded About Color!")
                break
            case "Dark Mode":
                
                let subviews = cell.subviews
                subviews[1].removeFromSuperview()
                
                cell.settingsLabel.text = "Dark Mode"
                cell.selectionStyle = .none
          
                darkSwitch = UISwitch(frame: CGRect(x: (cell.window?.bounds.width)! - 64, y: 5, width: 0, height: 0))
            
                if let dSwitch = darkSwitch{
                    dSwitch.addTarget(self, action: #selector(SettingsTableViewController.switchValueChanged(_:cell:)), for: .valueChanged)
                
                    print("DarkMode \(self.isDarkModeOn)")
                    if isDarkModeOn{
                        dSwitch.isOn = true
                        dSwitch.setOn(true, animated: true)
                        
                        saveIt.setDarkMode(isDarkMode: false)
                    } else {
                        dSwitch.isOn = false
                        dSwitch.setOn(false, animated: true)
                        
                        saveIt.setDarkMode(isDarkMode: true)
                    }
                    
                    cell.addSubview(dSwitch)
                }
                
                loadColor(cell: cell)
                break
            case "Disconnect":
                cell.settingsLabel.text = "Disconnect"
                
                loadColor(cell: cell)
                
                cell.settingsLabel.textColor = UIColor.red
                break
            default:
                break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableCellStyle[indexPath.row] == "Disconnect"{
            let alert = UIAlertController(title: "Are you sure you want to disconnect?", message: "After a car is disconnected you no longer have control over the car, and you need to connect a new one manually.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let tabView = self.presentingViewController
                let mainView = tabView?.presentingViewController?.childViewControllers[0]
                var mainCarView = tabView?.childViewControllers[0].childViewControllers[0] as? CarViewController
                //let mapView = tabView?.childViewControllers[2].childViewControllers[0] as! MapsViewController
          
                guard let carView = mainCarView else{ print("Car view could not load in Settings"); return}
                carView.stopProccesses()
                carView.permanentStop = true
                
                mainCarView = nil
                /*mainCarView.feedbackQueue.cancelAllOperations()
                mainCarView.motionManager.stopAccelerometerUpdates()
                NotificationCenter.default.removeObserver(mainCarView.self)
                
                if let clock = mainCarView.timer{
                    clock.invalidate(); mainCarView.timer = nil
                }*/
                
                //sleep(UInt32(1))
                self.dismiss(animated: false){
                    tabView?.dismiss(animated: false){
                        _ = mainView?.navigationController?.popToRootViewController(animated: true)
                        
                        guard let dummy = self.dummyModeEnabled else { print("ERROR: Could not load dummy mode!"); return}
                        if(!dummy){
                        
                            do{ try ClientSocket.sharedInstance.writeMessage(message: "quit") }    catch{ print("Error!") }
                        }
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.tableView.deselectRow(at: indexPath, animated: false)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            let feedback = UINotificationFeedbackGenerator()
            feedback.notificationOccurred(.warning)
        }
        else if tableCellStyle[indexPath.row] == "About"{
            
            let aboutView = self.storyboard?.instantiateViewController(withIdentifier: "aboutView") as! AboutTableViewController
            self.navigationController?.pushViewController(aboutView, animated: true)
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: Load and save colors

    func loadColor(cell: SettingsTableViewCell){
        
        //if let save = saveIt{
            print("in save in default")
            self.UIColors = saveIt.loadColor()
        //}
        
        for key in self.UIColors.keys{
            if key == "separator"{self.tableView.separatorColor = self.UIColors[key]}
            else if key == "textColor"{cell.settingsLabel.textColor = self.UIColors[key]}
            else if key == "rowBackgroundColor"{cell.backgroundColor = self.UIColors[key]}
            else if key == "backgroundColor"{self.tableView.backgroundColor = self.UIColors[key]}
        }
    }
    
    func setColor(cell: SettingsTableViewCell){
        if isDarkModeOn{
            
            self.UIColors.updateValue(#colorLiteral(red: 0.6250830889, green: 0.6250981092, blue: 0.6250900626, alpha: 1), forKey: "separator")
            self.UIColors.updateValue(#colorLiteral(red: 0.6250830889, green: 0.6250981092, blue: 0.6250900626, alpha: 1), forKey: "textColor")
            self.UIColors.updateValue(#colorLiteral(red: 0.6250830889, green: 0.6250981092, blue: 0.6250900626, alpha: 1), forKey: "detailTextColor")
            self.UIColors.updateValue(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1), forKey: "rowBackgroundColor")
            self.UIColors.updateValue(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1), forKey: "backgroundColor")
            
            saveIt.setUIColor(UIColors: self.UIColors)
            print("saved!")

        } else if !isDarkModeOn{
            
            self.UIColors.updateValue(#colorLiteral(red: 0.8209624887, green: 0.8209818602, blue: 0.8209714293, alpha: 1), forKey: "separator")
            self.UIColors.updateValue(UIColor.black, forKey: "textColor")
            self.UIColors.updateValue(#colorLiteral(red: 0.6286719441, green: 0.6286870837, blue: 0.6286789179, alpha: 1), forKey: "detailTextColor")
            self.UIColors.updateValue(UIColor.white, forKey: "rowBackgroundColor")
            self.UIColors.updateValue(UIColor.white, forKey: "backgroundColor")
            
            saveIt.setUIColor(UIColors: self.UIColors)
            print("saved!")
        }
        
    }
    
    @objc func switchValueChanged(_ sender: UISwitch!, cell: SettingsTableViewCell){
        
        if sender.isOn == true{
            self.isDarkModeOn = true
        }
        else if sender.isOn == false{
            self.isDarkModeOn = false
        }
        
        setColor(cell: cell)
        
        print("before save in switchvaluechanged")
        saveIt.saveData(); print("Saved from SettingsTableViewController!")
        self.tableView.reloadData(); print("reloaded!")
    }
    
    // MARK: IBAction
    
    @IBAction func DoneSettings(_ sender: Any) {
        
        navigationController!.popViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Update UI
    
    @objc func checkRotation(){
        let cell = self.tableView.visibleCells[2]
    
        if let dSwitch = darkSwitch{
            
            if let width = cell.window?.bounds.width{
                print("Changed Switch pos")
                dSwitch.frame = CGRect(x: width - 64, y: 5, width: 0, height: 0)
            }
        }
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    */
    

}

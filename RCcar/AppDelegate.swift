//
//  AppDelegate.swift
//  RcCars
//
//  Created by Thomas Visser on 02-11-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Mapbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //When this variable is set to true, the app will not connect with the car(s), and will only show dummy data.
    //Only set this to true if you can not demonstrate with a car.
    var dummyMode: Bool = true
    
    var window: UIWindow?
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let MapBoxAccesToken = "pk.eyJ1IjoidGhvbWFzdmlzc2VyIiwiYSI6ImNpeTJ5OGhkODAwMnQzM3J4ZmUzY29xNmoifQ.ibNXT7ROEU8yi5ae8MdIyQ"
    
    let locationManager = CLLocationManager()
    //let googleMapsAPIkey = "AIzaSyCSKZHyw3nRc-6_KSIWUjGzkrw0_UNhQhU"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setStatusBarBackgroundColor(color: UIColor(red: 0.082, green: 0.745, blue: 0.243, alpha: 1.00))
        MGLAccountManager.setAccessToken(MapBoxAccesToken)
        self.locationManager.requestWhenInUseAuthorization()
        //GMSServices.provideAPIKey(googleMapsAPIkey)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        //Disrupt the active processes before entering background
        let CarView = storyBoard.instantiateViewController(withIdentifier: "carView") as! CarViewController
        let MapsView = storyBoard.instantiateViewController(withIdentifier: "mapsView") as! MapsViewController
        
        //Disable processes for the Car View
        if let clock = CarView.timer{
            clock.invalidate(); CarView.timer = nil
            CarView.feedbackQueue.cancelAllOperations()
            CarView.motionManager.stopAccelerometerUpdates()
            CarView.updateAllowed = false
            do{ try ClientSocket.sharedInstance.writeMessage(message: "stop") } catch{ print("WriteError from applicationDidEnterBackground in AppDelegate")}
        }
        
        //Disable processes for the Maps View
        if let clock = MapsView.timer{
            clock.invalidate()
            MapsView.isUpdateData = false
            MapsView.gpsQueue.cancelAllOperations()
            MapsView.updateAllowed = false
        }
        do{ try ClientSocket.sharedInstance.writeMessage(message: "stop gps") } catch{ print("Error")}
        
        do{ try ClientSocket.sharedInstance.writeMessage(message: "halt") } catch{ print("WriteError from applicationDidEnterBackground in AppDelegate")}
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
        
        //Closes the streams and socket since it is no longer necessarily
        
        do{ try ClientSocket.sharedInstance.writeMessage(message: "quit") } catch{ print("WriteError from appicationWillTerminate in AppDelegate")}
        usleep(100)
        ClientSocket.sharedInstance.close()
        print("ClientSocket Closed!")
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "RcCars")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
        
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.isTranslucent = false
        navigationBarAppearance.tintColor = UIColor.white
        navigationBarAppearance.titleTextAttributes = [kCTForegroundColorAttributeName : UIColor.white] as [NSAttributedStringKey : Any]
        navigationBarAppearance.barTintColor = color
    }
    
}


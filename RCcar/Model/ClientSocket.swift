//
//  ClientSocket.swift
//  RcCars
//
//  Created by Thomas Visser on 05-11-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//
//  This ClientSocket class uses an API called "SwiftSocket" created by "danshevluk".
//  The source of the API can be found on github: https://github.com/swiftsocket/SwiftSocket
//
//  SwiftSocket API, most recent version: 2.0.1
//

import Foundation
import CoreFoundation
import SwiftSocket

class ClientSocket : NSObject, StreamDelegate {
    //static let sharedInstance = ClientSocket(address: "192.168.42.1", port: 8000)
    static let sharedInstance = ClientSocket(address: "169.254.161.31", port: 12001)
    //static let sharedInstance = ClientSocket(address: "192.168.178.25", port: 12001)
    
    var client: TCPClient?
    
    enum SocketError: Error{
        case ConnectionError
        case WriteError
        case ReadError
    }
    
    init(address: String, port: Int) {
        
        //Setup a TCPClient object to connect with host with the given IP and Port
        client = TCPClient(address: address, port: Int32(port))
    }
    
    //tries to open a connection with the given socket, and timeout on 3 seconds
    func openSocket() throws{
        
        if let clientje = client{
            switch clientje.connect(timeout: Int(1)){
            case .success:
                print("Succesfully connected!")
                return
            case .failure(let Error):
                print("Socket Error: \(Error)")
                throw SocketError.ConnectionError
            }
        }
    }
    
    //Writes the message to the socket
    func writeMessage(message: String) throws{
        
        let newMessage = "\(message)\r\n"
        
        let response = client?.send(string: newMessage)
        
        if let responses = response{
            switch responses{
            case .success:
                return
            case .failure(let Error):
                print("Socket Error: \(Error)")
                throw SocketError.WriteError
            }
        }
    }
    
    //Reads messages from the server socket
    func readMessage() -> String?{
        
        var message = ""
        
        let data = client?.read(1024*10, timeout: 1)
        
        if let dataa = data{
            if let response = String(bytes: dataa, encoding: .utf8){
                message = response
            }
        }
        
        print("returned from server: \(message)")
        return message
    }
    
    //closes the client socket
    func close(){
        client?.close()
    }
}

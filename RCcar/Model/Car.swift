//
//  Car.swift
//  RcCars
//
//  Created by Thomas Visser on 05-11-16.
//  Copyright © 2016 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Car{
    var image: UIImage
    var name: String
    var carID: String
    var ip: String
    var location: CLLocationCoordinate2D
    
    init(name: String, image: UIImage, carID: String, ip: String){
        self.name = name
        self.image = image
        self.carID = carID
        self.ip = ip
        self.location = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    }
}

//
//  MapDownloader.swift
//  RcCars
//
//  Created by Thomas Visser on 18-01-17.
//  Copyright © 2017 Thomas Visser. All rights reserved.
//

import Foundation
import CoreLocation
import Mapbox

class MapDownloader {
    static let instance = MapDownloader()
    
    static let map = MGLMapView()
    let locationManager = CLLocationManager()
    var offlinePack = 0
    let defaults = UserDefaults.standard
    
    init(){
        let gpsPos = locationManager.location?.coordinate
        
        if let pos = gpsPos{
            print("Loong \(pos.longitude) Laat: \(pos.latitude)")
            //MapDownloader.map.setCenter(CLLocationCoordinate2D(latitude: pos.latitude, longitude: pos.longitude), zoomLevel: 9, animated: false)
            
            print("center coor map \(MapDownloader.map.centerCoordinate)")
        }
    }
    
    func downloadMap(){
        
        print("in Download map!")
        let currentPos = self.locationManager.location?.coordinate
        
        if let pos = currentPos{
            
            //Calculate the offline area with device location as center.
            let sw = CLLocationCoordinate2D(latitude: pos.latitude + -0.053534180923332, longitude: pos.longitude + -0.050252343715617)
            let ne = CLLocationCoordinate2D(latitude: pos.latitude + 0.053467373644897, longitude: pos.longitude + 0.050252343715608)
            
            let bounds = MGLCoordinateBounds(sw: sw,  ne: ne)
            let region = MGLTilePyramidOfflineRegion(styleURL: MapDownloader.map.styleURL, bounds: bounds, fromZoomLevel: MapDownloader.map.zoomLevel, toZoomLevel: 20)
            
            // Store some data for identification purposes alongside the downloaded resources.
            let userInfo = ["name": "Local Area"]
            let context = NSKeyedArchiver.archivedData(withRootObject: userInfo)
            
            // Create and register an offline pack with the shared offline storage object.
            
            print("AddPack"); print("PACK COUNTER BEFORE \(MGLOfflineStorage.shared().packs?.count)")
            MGLOfflineStorage.shared().addPack(for: region, withContext: context) { (pack, error) in
                guard error == nil else {
                    // The pack couldn’t be created for some reason.
                    print("Error: \(error?.localizedDescription)")
                    return
                }
                
                // Start downloading.
                print("About to download pack!")
                pack!.resume()
            }
        }
    }
    
    func suspendPack(){
        
        let lastIndex = (MGLOfflineStorage.shared().packs?.count)! - 1
        if let pack = MGLOfflineStorage.shared().packs?[lastIndex]{
            pack.suspend();
            
            MGLOfflineStorage.shared().removePack(pack, withCompletionHandler: nil)
        }
    }
}

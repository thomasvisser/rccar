//
//  saveAppInfo.swift
//  RcCars
//
//  Created by Thomas Visser on 10-01-17.
//  Copyright © 2017 Thomas Visser. All rights reserved.
//

import UIKit

class SaveAppInfo: NSObject, NSCoding {
    var UIColors: Dictionary<String, UIColor> = Dictionary<String, UIColor>()
    var isDarkMode: Bool?
    let defaults = UserDefaults.standard
    
    override init(){}
    
    func setUIColor(UIColors: Dictionary<String, UIColor>){
        self.UIColors = UIColors
    }
    
    func setDarkMode(isDarkMode: Bool){
        self.isDarkMode = isDarkMode
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.UIColors = aDecoder.decodeObject(forKey: "UIColors") as! Dictionary<String, UIColor>
        self.isDarkMode = aDecoder.decodeBool(forKey: "isDarkMode")
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.UIColors, forKey: "UIColors")
        aCoder.encode(self.isDarkMode, forKey: "isDarkMode")
    }
    
    func loadColor() -> Dictionary<String, UIColor>{
        
        if let data: NSData = defaults.object(forKey: "UIColors") as? NSData{

            if let colorArray = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? Dictionary<String, UIColor>{
                self.UIColors = colorArray
            }
        }
        //self.UIColors = defaults.dictionary(forKey: "UIColors") as! Dictionary<String, UIColor>
        
        return self.UIColors
    }
    
    func loadDarkMode() -> Bool{
        
        if let data: NSData = defaults.object(forKey: "isDarkMode") as? NSData{
            if let dark = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? Bool{
                self.isDarkMode = dark;
            }
        }
        
        //if let darkMode = NSKeyedUnarchiver.unarchiveObject(with: data) as? Bool{
        //    self.isDarkMode = darkMode; print("Darkmode in read \(self.isDarkMode)")
        //}
        
        if let mode = isDarkMode {
            return mode
        }
        return false
    }
    
    func saveData(){
        let data = NSKeyedArchiver.archivedData(withRootObject: self.UIColors)
        defaults.set(data, forKey: "UIColors")
        defaults.synchronize()
        
        let darkData = NSKeyedArchiver.archivedData(withRootObject: isDarkMode ?? false)
        defaults.set(darkData, forKey: "isDarkMode")
        defaults.synchronize()
    }
}
